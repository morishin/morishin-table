#!/usr/bin/env python

from setuptools import setup


__author__ = 'Yusuke Miyazaki <miyazaki.dev@gmail.com>'
__version__ = '0.1'

requires = []

setup(
    name='morishin-table',
    version=__version__,
    author=__author__,
    author_email='miyazaki.dev@gmail.com',
    description='',
    install_requires=requires,
    py_modules = ['tabulate'],
    classifiers=[
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4'
    ]
)
