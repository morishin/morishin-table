# -*- coding: utf-8 -*-

import re
import unicodedata


_invisible_codes = re.compile(r"\x1b\[\d*m|\x1b\[\d*\;\d*\;\d*m")

def _strip_invisible(s):
    return re.sub(_invisible_codes, "", s)

def _ulen(string, double_width_types="WFA"):
    if isinstance(string, int):
        return len(str(string))
    elif isinstance(string, str):
        width = 0
        string = _strip_invisible(string)
        for c in string:
            char_width = unicodedata.east_asian_width(c)
            if char_width in double_width_types:
                width += 2
            else:
                width += 1
        return width
    else:
        raise TypeError

def _calc_width(column):
    width = 0
    for cell in column:
        width = max(width, _ulen(cell))
    return width

def _get_column(matrix, i):
    return [row[i] for row in matrix]

def _format_cell(cell, width):
    if isinstance(cell, int):
        fmt = "{0:>%dd}" % width
    elif isinstance(cell, str):
        width -= (_ulen(cell) - len(cell))
        fmt = "{0:<%ds}" % width
    return fmt.format(cell)

def _format_row(row, widths, vsep='|'):
    return (vsep + ' '
            + (' ' + vsep + ' ').join(map(lambda z: _format_cell(*z),
                                          zip(row, widths)))
            + ' ' + vsep)

def _format_separator(widths, vsep='+', hsep='='):
    return (vsep + hsep
            + (hsep + vsep + hsep).join(map(lambda w: hsep * w, widths))
            + hsep + vsep + '\n')

def _format_header(row, widths):
    header = _format_row(row, widths)
    return header + '\n'

def tabulate(data, headers):
    num_columns = len(data[0])
    widths = [_calc_width(_get_column([headers] + data, i))
              for i in range(num_columns)]
    header = _format_header(headers, widths)
    separator1 = _format_separator(widths, hsep='-')
    separator2 = _format_separator(widths)
    body = '\n'.join(map(lambda r: _format_row(r, widths), data)) + '\n'
    return separator1 + header + separator2 + body + separator1

if __name__ == '__main__':
    import json
    with open("table.json") as f:
        table = json.load(f)
    print(tabulate(table[1:], headers=table[0]))
